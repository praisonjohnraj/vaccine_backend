const express = require('express');
const router = express.Router();
const db = require("./mongo");
const { Validator } = require('node-input-validator');



router.post("/insert", async (req, res) => {

    let db1 = db.get().collection("USER_TABLE");
    let db2 = db.get().collection("USER_DETAILS");

    let err = {};
    const v = new Validator(req.body.dose_details,
        {
            _id: 'required|string',
            DOSE_NUMBER: 'required|minLength:1',
            HOSPITAL: 'required|string',
            HOSPITAL_id: 'required|string',
            PLACE: 'required|minLength:1',
            TIME: 'required|string',
            DATE: 'required|string'
        },
    );

    const matched = await v.check();

    await v.check().then(function (matched) {
        err = {
            matched: matched,
            errors: v.errors
        }
    });

    let _id = req.body._id;


    if (matched) {

        // db1 check id is there or not in usertable
        db1
            .find({ _id })
            .toArray()
            .then(async result => {
                console.log(result);
                if (result.length > 0) {
                    let id = result[0]._id;
                    //check id is there or not in userdetails
                    let x = await db2.find({ _id: id }).toArray();
                    if (x.length === 0) {
                        db2.insertOne({ _id: id, dose_details: [req.body.dose_details] })
                    }
                    else {
                        console.log("xxxxx----", x)
                        let flag = false;
                        let flag1 = false;

                        let doseArray = x[0].dose_details;

                        let doseno = parseInt(ids)
                        for (let i = 1; i < doseArray.length; i++) {
                            if (doseArray[i]._id === ids) {
                                flag = true;
                                break;
                            }
                        }

                        if (flag === true)
                            res.json({ Status: 0, msg: "Already exists" });
                        else {
                            // check  id in db2 user details
                            let y = await db2.find({ _id: ids }).toArray();
                            console.log(y)
                            if (y.length === 0 && flag === false) {
                                await db2.updateOne({ _id: id }, { $push: { dose_details: req.body.dose_details } }, { $upsert: true })
                                    .then(x => console.log(x))
                                res.json("Details added succesfully")
                            }
                            else {
                                res.json("no of dose already exist")
                            } let ids = req.body.dose_details._id;


                            // add dose details in user details

                        }

                    }
                } else {
                    res.json({ Status: 2, info: "requested ID not exist in the User table" })

                }

            });
    }
    else {
        res.json({ Status: 3, err: err });

    }
});
router.post("/insertnew", async (req, res) => {

    let db2 = db.get().collection("USER_DETAILS");
    let err = {};
    // console.log(req.body.dose_details)
    const v = new Validator(req.body.dose_details,
        {
            _id: 'required|string',
            DOSE_NUMBER: 'required|minLength:1',
            HOSPITAL: 'required|string',
            HOSPITAL_id: 'required|string',
            PLACE: 'required|minLength:1',
            TIME: 'required|string',
            DATE: 'required|string'
        },
    );

    const matched = await v.check();

    await v.check().then(function (matched) {
        err = {
            matched: matched,
            errors: v.errors
        }
    });
    console.log("Praison")
    if (matched) {
        console.log(("result----"))

        let id = req.body._id;
        let result = await db2.find({ _id: id }).toArray();
        if (result.length === 0) {
            console.log("1111-------", result);
            if (req.body.dose_details.DOSE_NUMBER === 1) {
                db2.insertOne({ _id: id, dose_details: [req.body.dose_details] })
                res.json({ Status: 1, info: "Details added successfully1" });
            }
            else res.json({ Status: 0, info: " Not Directly add : 4" });

        }
        else {
            dosearray = result[0].dose_details;
            var flag1 = false, flag = false;
            doseid = parseInt(req.body.dose_details._id);
            idss = parseInt(doseid) - 1;
            let length = dosearray.length;

            if (parseInt(dosearray[length - 1]._id) === parseInt(doseid))
                flag = true;

            if (parseInt(dosearray[length - 1]._id) === parseInt(idss))
                flag1 = true;

            if (flag === true) {
                res.json({ Status: 0, info: "Dose Already Exist" });
            }
            else if (flag1 === false) {
                res.json({ Status: 0, info: "Previous Dose not available" });
            }
            else {
                db2.updateOne({ _id: id }, { $push: { dose_details: req.body.dose_details } }, { $upsert: true })
                    .then(x => console.log(x))
                res.json({ Status: 1, info: "Details added succesfully"})

            }

        }

    } else {
        res.json({ Status: 3, err: err });

    }

});


module.exports = router;