const express = require('express');
const router = express.Router();
const db = require("./mongo");
const { Validator } = require('node-input-validator');


/*router.post("/insert", async (req, res) => {

    console.log(check.js);

    let data = {
        "_id": req.body._id, "FIRST_NAME": req.body.FIRST_NAME, "SECOND_NAME": req.body.SECOND_NAME, "MAIL_ID": req.body.MAIL_ID,
        "MOBILE": req.body.MOBILE, "ROLE": req.body.ROLE, "DEPARTMENT": req.body.DEPARTMENT, "YEAR": req.body.YEAR,
        "SECTION": req.body.SECTION, "PASSWORD": req.body.PASSWORD
    };
    var mailformat = /\S+@\S+\.\S+/;
    var strings = '^ [A - Za - z] + $';
    var password = '^[A-Za-z0-9]+$';
    if (data._id.length > 0 && strings.test(data.FIRST_NAME) && strings.test(data.SECOND_NAME) && mailformat.test(data.MAIL_ID) && data.MOBILE.length > 0 && data.ROLE.length > 0 && data.DEPARTMENT.length > 0 && data.YEAR.length > 0 && data.SECTION.length > 0 && password.test(data.PASSWORD)) {
        console.log(data);

        let db1 = db.get().collection("USER_TABLE");
        db1.insertOne(data).then((result) => {
            console.log(result);
            if (result.acknowledged === true) {
                res.json({ Status: 1 });
            } else {
                res.json({ Status: 0 });
            }
        });
    } else {
        res.json({ Status: 2 });
    }


});*/


router.post("/login", async (req, res) => {
    let data = {
        "MAIL_ID": req.body.MAIL_ID, "PASSWORD": req.body.PASSWORD
    };
    let db1 = db.get().collection("USER_TABLE");

    console.log(data);
    const v = new Validator(req.body,
        {
            MAIL_ID: 'required|email',
            NEWPASSWORD: 'required'
        },
    );
    await v.check().then(function (matched) {
        err = {
            matched: matched,
            errors: v.errors
        }
    });
    if (matched) {
        db1
            .find(data)
            .toArray()
            .then((result) => {
                /*  console.log(result);*/
                if (result.length > 0) {
                    res.json({ result: result[0], Status: 1 });
                } else {
                    res.json({ Status: 0 });
                }
            });
    } else {
        res.json({ Status: 2, err: err });

    }

});

router.post("/changepwd", async (req, res) => {

    let PASSWORD = req.body.PASSWORD;
    let NEWPASSWORD = req.body.NEWPASSWORD;
    let db1 = db.get().collection("USER_TABLE");
    const v = new Validator(req.body,
        {
            PASSWORD: 'required|string',
            NEWPASSWORD: 'required'
        },
    );

    const matched = await v.check();
    let err = {};
    await v.check().then(function (matched) {
        err = {
            matched: matched,
            errors: v.errors
        }
    });
    if (matched) {

        db1
            .find({ "PASSWORD": PASSWORD })
            .toArray()
            .then((result) => {
                console.log(result);
                if (result.length > 0) {
                    db1.update(result[0], { $set: { "PASSWORD": NEWPASSWORD } }, { upsert: true }).then((result) => {
                        console.log(result);
                        if (result.acknowledged === true) {
                            res.json({ Status: 1 });
                        } else {
                            res.json({ Status: 0 });
                        }
                    });

                } else {
                    res.json({ Status: 0 });

                }
            });
    } else {
        res.json({ Status: 2, err: err });

    }

});

router.post("/deleteuser", async (req, res) => {
    let data = {
        "_id": req.body._id
    };
    console.log(data);
    const v = new Validator(req.body,
        {
            _id: 'required|string',
        },
    );

    const matched = await v.check();
    let err = {};
    await v.check().then(function (matched) {
        err = {
            matched: matched,
            errors: v.errors
        }
    });

    let db1 = db.get().collection("USER_DETAILS");
    if (matched) {

        db1
            .find(data)
            .toArray()
            .then((result) => {
                console.log(result);
                if (result.length > 0) {
                    db1
                        .remove(result[0])
                        .then((result) => {
                            console.log(result);
                            if (result.deletedCount === 1) {
                                res.json({ Status: 1 });
                            } else {
                                res.json({ Status: 0 });
                            }
                        });
                }
                else {
                    res.json({ Status: 0 });


                }
            });
    }
    else {
        res.json({ Status: 2, err: err });

    }

});


router.post("/updateuser", async (req, res) => {

    let data = {
        "_id": req.body._id, "FIRST_NAME": req.body.FIRST_NAME, "SECOND_NAME": req.body.SECOND_NAME, "MAIL_ID": req.body.MAIL_ID,
        "MOBILE": req.body.MOBILE, "ROLE": req.body.ROLE, "DEPARTMENT": req.body.DEPARTMENT, "YEAR": req.body.YEAR,
        "SECTION": req.body.SECTION, "PASSWORD": req.body.PASSWORD
    };
    let _id = req.body._id;
    let db1 = db.get().collection("USER_TABLE");
    let err = {};
    const v = new Validator(req.body,
        {
            _id: 'required|string',
            FIRST_NAME: 'required',
            SECOND_NAME: 'required',
            MAIL_ID: 'required|email',
            MOBILE: 'required|integer|minLength:10',
            ROLE: 'required|string',
            DEPARTMENT: 'required|string',
            YEAR: 'required|integer|maxLength:4|minLength:4',
            SECTION: 'required|string',
            PASSWORD: 'required'
        },
    );
    const matched = await v.check();

    await v.check().then(function (matched) {
        err = {
            matched: matched,
            errors: v.errors
        }
    });
    if (matched) {

        db1
            .find({ _id })
            .toArray()
            .then((result) => {
                console.log(result);
                if (result.length > 0) {
                    db1
                        .update(
                            { _id: req.body._id },
                            {
                                $set: {
                                    "FIRST_NAME": req.body.User_Name, "SECOND_NAME": req.body.SECOND_NAME, "MAIL_ID": req.body.MAIL_ID,
                                    "MOBILE": req.body.MOBILE, "ROLE": req.body.ROLE, "DEPARTMENT": req.body.DEPARTMENT, "YEAR": req.body.YEAR,
                                    "SECTION": req.body.SECTION, "PASSWORD": req.body.PASSWORD
                                }
                            },
                            { upsert: true }

                        )
                        .then((result) => {
                            console.log(result);
                            if (result.modifiedCount === 1) {
                                res.json({ Status: 1 });
                            } else {
                                res.json({ Status: 0 });
                            }
                        });
                } else {
                    res.json({ Status: 0 });

                }
            });
    }
    else {
        res.json({ Status: 2, err: err });

    }

});

router.post("/insert", async (req, res) => {

    let db1 = db.get().collection("USER_TABLE");
    let err = {};
    const v = new Validator(req.body,
        {
            _id: 'required|string',
            FIRST_NAME: 'required|minLength:3',
            SECOND_NAME: 'required|minLength:3',
            MAIL_ID: 'required|email',
            MOBILE: 'required|integer|minLength:10',
            ROLE: 'required|string',
            DEPARTMENT: 'required|string',
            YEAR: 'required|integer|maxLength:4|minLength:4',
            SECTION: 'required|string',
            PASSWORD: 'required'
        },
    );
    const matched = await v.check();

    await v.check().then(function (matched) {
        err = {
            matched: matched,
            errors: v.errors
        }
    });


    let data = {
        "_id": req.body._id, "FIRST_NAME": req.body.FIRST_NAME, "SECOND_NAME": req.body.SECOND_NAME, "MAIL_ID": req.body.MAIL_ID,
        "MOBILE": req.body.MOBILE, "ROLE": req.body.ROLE, "DEPARTMENT": req.body.DEPARTMENT, "YEAR": req.body.YEAR,
        "SECTION": req.body.SECTION, "PASSWORD": req.body.PASSWORD
    };

    if (matched) {
        db1.insertOne(data).then((result) => {
            console.log(result);
            if (result.acknowledged === true) {
                res.json({ Status: 1 });
            } else {
                res.json({ Status: 0 });
            }
        });
    }
    else {
        res.json({ Status: 2, err: err });

    }

});
router.post("/deletedose", async (req, res) => {
    let db1 = db.get().collection("USER_DETAILS");
    let id = req.body._id;
    let idss = req.body.dose_details._id;
    db1
        .find({ "_id": id })
        .toArray()
        .then(async (result) => {
            console.log(result, result.length);
            if (result.length > 0) {
                await db1.
                    updateOne({ _id: id }, { "$pull": { "dose_details": { _id: idss } } })
                    .then(x => console.log(x))
                    .catch(e => console.log(e))

            }
            else {
                res.json({ Status: 0 });
            }
            await res.json({ Status: 1 })
        });
});
router.post("/update1", async (req, res) => {
    let db1 = db.get().collection("USER_DETAILS");
    let id = req.body._id;
    let idss = req.body.dose_details._id;
    db1
        .find({ "_id": id })
        .toArray()
        .then(async (result) => {
            console.log(result, result.length);
            if (result.length > 0) {
                await db1.
                    updateOne({ _id: id }, { "$pull": { "dose_details": { _id: idss } } })
                    .then(x => console.log(x))
                    .catch(e => console.log(e))

            }
            else {
                res.json({ Status: 0 });
            }
            await res.json({ Status: 1 })
        });
});
router.post("/update", async (req, res) => {

    let data = {
        "_id": req.body.dose_details._id, "DOSE_NUMBER": req.body.dose_details.DOSE_NUMBER, "HOSPITAL": req.body.dose_details.HOSPITAL, "HOSPITAL_id": req.body.dose_details.HOSPITAL_id,
        "PLACE": req.body.dose_details.PLACE, "TIME": req.body.dose_details.TIME, "DATE": req.body.dose_details.DATE
    };
    console.log(data)
    let _id = req.body._id;
    let db1 = db.get().collection("USER_DETAILS");
    let err = {};
    /*  const v = new Validator(req.body,
          {
              _id: 'required|string',
              DOSE_NUMBER: 'required|minLength:5|integer',
              HOSPITAL: 'required|string',
              HOSPITAL_id: 'required',
              PLACE: 'required|minLength:1',
              TIME: 'required|string',
              DATE: 'required|string'
            
          },
      );
      const matched = await v.check();
  
      await v.check().then(function (matched) {
          err = {
              matched: matched,
              errors: v.errors
          }
      });
      */
    const matched = true;
    if (matched) {

        db1
            .find({ _id })
            .toArray()
            .then((result) => {
                // console.log(result);
                if (result.length > 0) {
                    db1
                        .update(
                            { _id: req.body._id, "dose_details._id": req.body.dose_details._id },
                            {
                                $set: {
                                    "dose_details.$.DOSE_NUMBER": req.body.dose_details.DOSE_NUMBER, "dose_details.$.HOSPITAL": req.body.dose_details.HOSPITAL, "dose_details.$.HOSPITAL_id": req.body.dose_details.HOSPITAL_id,
                                    "dose_details.$.PLACE": req.body.dose_details.PLACE, "dose_details.$.TIME": req.body.dose_details.TIME, "dose_details.$.DATE": req.body.dose_details.DATE
                                }
                            },
                            { upsert: false }

                        )
                        .then((result) => {
                            console.log(result);
                            if (result.modifiedCount === 1) {
                                res.json({ Status: 1 });
                            } else {
                                res.json({ Status: 0 });
                            }
                        });
                } else {
                    res.json({ Status: 0 });

                }
            });
    }
    else {
        res.json({ Status: 2, err: err });

    }

});
router.post("/view", async (req, res) => {

    let db1 = db.get().collection("USER_DETAILS");
    let _id =  req.body._id;
    console.log(_id)
    db1
        .find({_id})
        .toArray()
        .then((result) => {
            console.log(result);
            if (result.length > 0) {
                res.json({ result: result[0].dose_details, Status: 1 });
            } else {
                res.json({ Status: 0 });
            }
        });
});
module.exports = router;


