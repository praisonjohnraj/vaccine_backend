const express = require("express");
const morgan = require("morgan");
const bodyparser = require("body-parser");
const query = require("./common");
const studentdetails = require("./student");
const Login =require("./login");
var cors = require("cors");
const app = express();


const mongo = require("./mongo");
mongo.connect(function (err, client) {
    if (err) {
        console.log(err);
    }
    else {
        console.log("login Database Connected");
    }
});

app.use(morgan("dev"));
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(cors());
app.use("/common", query);
app.use("/student",studentdetails);
app.use("/login", Login);
const port = 7000;
app.listen(port, () => console.log('server started on port ' + port));