const express = require('express');
const router = express.Router();
const db = require("./mongo");

    router.post("/check", async (req, res) => {
        let data = {
            "_id": req.body._id,
            "PASSWORD": req.body.PASSWORD

        };
        console.log(data);
        let db1 = db.get().collection("USER_TABLE");

        db1
            .find({data})
            .toArray()
            .then((result) => {
                console.log(result);
                if (result.length > 0) {
                    res.json({ result: result, Status: 1 });
                } else {
                    res.json({ Status: 0 });
                }
            });
        });

router.post("/find", async (req, res) => {
    let data = {
        "_id": req.body._id,
    };
    console.log(req.body._id);
    let db1 = db.get().collection("USER_DETAILS");

    db1
        .find(data)
        .toArray()
        .then((result) => {
            console.log(result);
            if (result.length > 0) {
                res.json({ result: result, Status: 1 });
            } else {
                res.json({ Status: 0 });
            }
        });
});

module.exports = router;